class CourbeElliptique {

    constructor(a, b) {
        this.a = a;
        this.b = b;
        if ( 4 * a ** 3 + 27 * 2 ** 2 === 0 ) {
            throw new Error("Courbe invalide");
        }
    }

    compare(curveToCompare) {
        if(
            this === curveToCompare
            || (this.a === curveToCompare.a && this.b === curveToCompare.b)
        ) return true;
        return false;
    }

    testPoint(x, y) {
        if ( Math.round(y ** 2 - x ** 3) === Math.round(this.a * x + this.b )) {
            console.log(`Le point(x: ${x}, y: ${y} fait partie de la courbe`);
        } else {
            console.log(`Le point(x: ${x}, y: ${y} fait PAS partie de la courbe`);
        }
        // console.log(y ** 2 - x ** 3);
        // console.log(this.a * x + this.b);
    }

    toString() {
        return this;
    }
}

const courbe1 = new CourbeElliptique(5, 9);
const courbe2 = new CourbeElliptique(10,-3);
const courbe3 = new CourbeElliptique(5,9);

//Test si un point fait partie de la courbe ==> vrai
courbe1.testPoint(-0.86883, 2);
//Test si un point fait partie de la courbe ==> faux
courbe1.testPoint(-0.86883, 8);

//Compare 2 courbes ==> true
console.log(`La courbe 1 est-elle identique à la courbe 3 : ${courbe1.compare(courbe3)}`);
//Compare 2 courbes ==> false
console.log(`La courbe 1 est-elle identique à la courbe 2 : ${courbe1.compare(courbe2)}`);

// Renvoi les paramètre de la courbe
console.log(courbe1.toString());

