## Exercice 1.3.4 : Représenter une courbe elliptique
Définir une classe CourbeElliptique qui prend pour paramètres a et b et vérifie qu’ils satisfont la propriété présentée dans le cours ( 4a³+27b² ≠ 0 )

Par exemple, en python:

`class CourbeElliptique:
	def __init__(self, a, b):
		self.a = a
		self.b = b
		if 4*a**3+27*b**2 == 0 
			raise ValueError('({}, {}) n\'est pas une courbe valide'.format(x, y))`
			
Ajouter à la classe courbe elliptique une fonction qui permette de comparer si deux courbes sont égales ( En Python __eq__ permettra d'utiliser l'opérateur d'égalité == )

Ajouter une fonction testPoint(self,x, y )(en python) ou testPoint(x,y) (en javascript) qui  vérifie si un point appartient à la courbe

Ajouter une fonction qui retourne une chaîne de caractères avec les paramètres de la courbe
